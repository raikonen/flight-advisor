<?php

use App\Http\Controllers\AirportsController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CitiesController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\FlightController;
use App\Http\Middleware\Authorise;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v1/', 'middleware' => 'auth:api'], function () {
    Route::group(['middleware' => Authorise::class], function () {
        Route::post('airports/import', [AirportsController::class, 'import']);
        Route::post('cities/{id}/airports', [AirportsController::class, 'store']);
        Route::post('flights/import', [FlightController::class, 'import']);
        Route::post('flights', [FlightController::class, 'store']);
        Route::post('cities', [CitiesController::class, 'store']);
    });

    Route::post('flights/cheapest', [FlightController::class, 'cheapest']);
    Route::get('cities', [CitiesController::class, 'index']);

    Route::post('cities/{id}/comments', [CommentsController::class, 'store']);
    Route::put('comments/{id}', [CommentsController::class, 'update']);
    Route::delete('comments/{id}', [CommentsController::class, 'destroy']);
});

Route::post('register', [RegisterController::class, 'register']);
Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LoginController::class, 'logout']);
