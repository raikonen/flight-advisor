<?php

namespace Tests\Unit;

use App\Services\ImportService\AirportsImporter;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use SplFileObject;
use Tests\Invokable;

class AirportImporterTest extends TestCase
{
    private $airports_importer;
    /**
     * @var MockObject|callable
     */
    private $airport_repository;

    /**
     * @var MockObject|callable
     */
    private $cities_finder;

    /**
     * @var MockObject|SplFileObject
     */
    private $file;

    /**
     * @var array[]
     */
    private $import_data;

    protected function setUp(): void
    {
        $this->markTestSkipped('Spl cant be mocked.');
        parent::setUp();

        $this->airport_repository = $this->createPartialMock(Invokable::class, ['__invoke']);
        $this->cities_finder = $this->createPartialMock(Invokable::class, ['__invoke']);

        $this->file = $this->createMock(SplFileObject::class);

        $this->import_data = [
            [
                316,
                "Oostmalle Air Base",
                "Zoersel",
                "Belgium",
                "\N",
                "EBZR",
                "51.264702",
                "4.75333,53,1",
                "E",
                "Europe/Brussels",
                "airport",
                "OurAirports"
            ]
        ];

        $this->airports_importer = new AirportsImporter(
            $this->airport_repository,
            $this->cities_finder
        );
    }

    public function testWillImportData()
    {
        $this
            ->cities_finder
            ->expects($this->once())
            ->method('__invoke')
            ->willReturn(
                [
                    1 =>
                    [
                        'id' => 1,
                        'name' => "Zoersel",
                        'country' => "Belgium",
                    ]
                ]
            );

        $this
            ->file
            ->expects($this->atLeast(2))
            ->method('fgetcsv')
            ->willReturnOnConsecutiveCalls(
                $this->import_data[0],
                null
            );

        $this
            ->airport_repository
            ->expects($this->once())
            ->method('__invoke')
            ->with(
                [
                    'airport_id' => 316,
                     'name' => "Oostmalle Air Base",
                     'city_id' => 1,
                     'country' => "Belgium",
                     'iata' => "\N",
                     'icao' => "EBZR",
                     'latitude' => 51.264702,
                     'longitude' => 4.75333531,
                     'altitude' => "E",
                     'timezone' => "Europe/Brussels",
                     'type' =>"airport",
                     'source' =>"OurAirports"
                ]
            );

        $this
            ->airports_importer
            ->import($this->file);
    }

    public function testWillNotImportDataIfCityNotExists()
    {
        $this
            ->cities_finder
            ->expects($this->once())
            ->method('__invoke')
            ->willReturn([]);

        $this
            ->file
            ->expects($this->atLeast(2))
            ->method('fgetcsv')
            ->willReturnOnConsecutiveCalls(
                $this->import_data[0],
                null
            );

        $this
            ->airport_repository
            ->expects($this->never())
            ->method('__invoke');

        $this
            ->airports_importer
            ->import($this->file);
    }
}
