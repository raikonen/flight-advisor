<?php

namespace Tests\Unit;

use App\CoordinateInterface;
use App\Utils\DistanceCalculator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class DistanceCalculatorTest extends TestCase
{
    private $distance_calculator;

    /**
     * @var CoordinateInterface|MockObject
     */
    private $first_location;

    /**
     * @var CoordinateInterface|MockObject
     */
    private $second_location;

    protected function setUp(): void
    {
        parent::setUp();

        $this->first_location = $this->createMock(CoordinateInterface::class);
        $this->second_location = $this->createMock(CoordinateInterface::class);

        $this->distance_calculator = new DistanceCalculator();
    }

    /**
     * @dataProvider provideLocations
     */
    public function testWillCalculateDistance(
        float $first_latitude,
        float $first_longitude,
        float $second_latitude,
        float $second_longitude,
        float $expected_distance
    ) {
        $this
            ->first_location
            ->expects($this->atLeast(1))
            ->method('latitude')
            ->willReturn($first_latitude);

        $this
            ->second_location
            ->expects($this->atLeast(1))
            ->method('latitude')
            ->willReturn($second_latitude);

        $this
            ->first_location
            ->expects($this->atLeast(1))
            ->method('longitude')
            ->willReturn($first_longitude);

        $this
            ->second_location
            ->expects($this->atLeast(1))
            ->method('longitude')
            ->willReturn($second_longitude);

        $distance = $this
            ->distance_calculator
            ->getDistance(
                $this->first_location,
                $this->second_location
            );

        $this->assertSame(
            $expected_distance,
            $distance
        );
    }

    public function provideLocations()
    {
        return [
            [
                1, 1, 1, 1, 0 // same location
            ],
            [
                44.80401, 20.46513, 43.32472, 21.90333, 200.64 // Belgrade - Nis
            ],
            [
                44.80401, 20.46513, 45.82972, 20.46528, 114.05 // Belgrade - Kikinda
            ],
            [
                50.85045, 4.34878, 4.51667, 12.03333, 5201.43 // Brussels - Nkoteng
            ],
            [
                25.05823, 77.34306, 34.4, 132.45, 5356.51 // Nassau - Hiroshima
            ],
        ];
    }
}
