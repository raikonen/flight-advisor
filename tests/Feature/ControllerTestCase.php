<?php

namespace Tests\Feature;

use Illuminate\Testing\TestResponse;
use Tests\TestCase;

abstract class ControllerTestCase extends TestCase
{
    protected function assertErrorMessage(TestResponse $response, array $message): void
    {
        $decoded_response = $response->decodeResponseJson();

        $this->assertArrayHasKey('message', $decoded_response);
        $this->assertArrayHasKey('errors', $decoded_response);

        $this->assertSame(
            "The given data was invalid.",
            $decoded_response['message']
        );

        $this->assertSame(
            $message,
            $decoded_response['errors']
        );
    }

    protected function assertResponse(
        TestResponse $response,
        $user,
        array $properties
    ): void
    {
        $decoded_response = $response->decodeResponseJson();

        foreach ($properties as $property) {
            $this->assertSame(
                $user->$property,
                $decoded_response[0][$property]
            );
        }
    }
}
