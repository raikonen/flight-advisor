<?php

namespace Tests\Feature;

use App\City;
use App\Comment;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;

class CommentsControllerTest extends ControllerTestCase
{
    use RefreshDatabase;

    private $api_token;
    private $user;
    private $city;

    protected function setUp(): void
    {
        parent::setUp();

        $this->api_token = Str::random(60);

        $this->user = User::create([
            'username' => 'johndoe',
            'first_name' => 'John',
            'last_name' => 'Doe',
            'password' => bcrypt('easytoremmember'),
        ]);

        $this->city = City::create([
            'name' => 'Belgrade',
            'country' => 'Serbia',
            'description' => 'description'
        ]);

        $this->user->api_token = $this->api_token;
        $this->user->save();
    }

    public function testWillReturn403IfTryToUpdateCommentCreatedByOtherUser()
    {
        $creator = $this->createotherUser();

        $comment = Comment::create([
            'city_id' => $this->city->id,
            'content' => 'NOT UPDATED COMMET',
            'created_by_user_id' => $creator->id,
        ]);

        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->putJson(
                "/api/v1/comments/{$comment->id}",
                ['content' => "EDITED BY USER {$this->user->name}"]
            );

        $this->assertSame(
            403,
            $response->status()
        );
    }

    public function testWillReturn403IfTryToDeleteCommentCreatedByOtherUser()
    {
        $creator = $this->createotherUser();

        $comment = Comment::create([
            'city_id' => $this->city->id,
            'content' => 'NOT UPDATED COMMET',
            'created_by_user_id' => $creator->id,
        ]);

        $this->assertSame(
            1,
            Comment::all()->count()
        );

        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->deleteJson(
                "/api/v1/comments/{$comment->id}"
            );

        $this->assertSame(
            403,
            $response->status()
        );

        $this->assertSame(
            1,
            Comment::all()->count()
        );
    }

    public function testWillDeleteComment()
    {
        $comment = Comment::create([
            'city_id' => $this->city->id,
            'content' => 'NOT UPDATED COMMET',
            'created_by_user_id' => $this->user->id,
        ]);

        $this->assertSame(
            1,
            Comment::all()->count()
        );

        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->deleteJson(
                "/api/v1/comments/{$comment->id}"
            );

        $this->assertSame(
            204,
            $response->status()
        );

        $this->assertSame(
            0,
            Comment::all()->count()
        );
    }

    public function testWillUpdateComment()
    {
        $comment = Comment::create([
            'city_id' => $this->city->id,
            'content' => 'NOT UPDATED COMMET',
            'created_by_user_id' => $this->user->id,
        ]);

        $this->assertSame(
            1,
            Comment::all()->count()
        );

        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->putJson(
                "/api/v1/comments/{$comment->id}",
                ['content' => 'UPDATED COMMENT']
            );

        $this->assertSame(
            200,
            $response->status()
        );

        $comment = Comment::all()->first();

        $decoded_response = $response->decodeResponseJson();

        $this->assertSame(
            (int) $comment->city_id,
            (int) $decoded_response['city_id']
        );

        $this->assertSame(
            $comment->content,
            'UPDATED COMMENT'
        );
        $this->assertSame(
            (int) $comment->created_by_user_id,
            (int) $decoded_response['created_by_user_id']
        );
    }

    public function testWillAddCommentOnCity()
    {
        $this->assertSame(
            0,
            Comment::all()->count()
        );

        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->postJson(
                "/api/v1/cities/{$this->city->id}/comments",
                ['content' => 'TEST #1']
            );

        $this->assertSame(
            201,
            $response->status()
        );

        $this->assertSame(
            1,
            Comment::all()->count()
        );

        $comment = Comment::all()->first();

        $decoded_response = $response->decodeResponseJson();

        $this->assertSame(
            (int) $comment->city_id,
            (int) $decoded_response['city_id']
        );

        $this->assertSame(
            $comment->content,
            $decoded_response['content']
        );
        $this->assertSame(
            (int) $comment->created_by_user_id,
            (int) $decoded_response['created_by_user_id']
        );
    }

    public function testWillReturnBAdRequestIfContentNotProvided()
    {
        $this->assertSame(
            0,
            Comment::all()->count()
        );

        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->postJson(
                "/api/v1/cities/{$this->city->id}/comments",
                ['text' => 'TEST #1']
            );

        $this->assertSame(
            422,
            $response->status()
        );

        $this->assertSame(
            0,
            Comment::all()->count()
        );

        $this->assertErrorMessage($response, ['content' => ['The content field is required.']]);
    }

    public function testWillReturn404CityNotExists()
    {
        $this->assertSame(
            0,
            Comment::all()->count()
        );

        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->postJson(
                "/api/v1/cities/666/comments",
                ['content' => 'TEST #1']
            );

        $this->assertSame(
            404,
            $response->status()
        );

        $this->assertSame(
            0,
            Comment::all()->count()
        );
    }

    /**
     * @return mixed
     */
    private function createotherUser()
    {
        $creator = User::create([
            'username' => 'johndoe2',
            'first_name' => 'John2',
            'last_name' => 'Doe2',
            'password' => bcrypt('easytoremmember'),
        ]);
        return $creator;
    }
}
