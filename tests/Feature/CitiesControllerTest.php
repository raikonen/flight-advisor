<?php

namespace Tests\Feature;

use App\City;
use App\Comment;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;

class CitiesControllerTest extends ControllerTestCase
{
    use RefreshDatabase;

    private $api_token;
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->api_token = Str::random(60);

        $this->user = User::create([
            'username' => 'johndoe',
            'first_name' => 'John',
            'last_name' => 'Doe',
            'password' => bcrypt('easytoremmember'),
        ]);

        $this->user->role = User::ROLE_ADMIN;
        $this->user->save();

        $this->user->api_token = $this->api_token;
        $this->user->save();
    }

    public function testWillReturnBadRequestIfTTryToCreateCityWithSameName()
    {
        $this->createCities();

        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->postJson(
                '/api/v1/cities', [
                    'name' => 'Belgrade',
                    'country' => 'Serbia',
                    'description' => 'test',
                ]
            );

        $this->assertSame(
            422,
            $response->status()
        );


        $this->assertSame(
            5,
            City::all()->count()
        );

        $this->assertErrorMessage(
            $response,
            ['name' => ["The name has already been taken."]]
        );
    }

    public function testWillReturn403IfUserIsNotAdmin()
    {
        $this->user->role = User::ROLE_REGULAR;
        $this->user->save();

        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->postJson(
                '/api/v1/cities', [
                    'name' => 'Subotica',
                    'country' => 'Serbia',
                    'description' => 'test',
                ]
            );

        $this->assertSame(
            403,
            $response->getStatusCode()
        );
    }

    public function testWillCreateCity()
    {
        $this->assertSame(
            0,
            City::all()->count()
        );

        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->postJson(
                '/api/v1/cities', [
                    'name' => 'Subotica',
                    'country' => 'Serbia',
                    'description' => 'test',
                ]
            );

        $this->assertSame(
            201,
            $response->status()
        );

        $this->assertSame(
            1,
            City::all()->count()
        );

        $city = City::all()->first();

        $decoded_response = $response->decodeResponseJson();

        foreach (['id', 'name', 'country', 'description'] as $property) {
            $this->assertSame(
                $city->$property,
                $decoded_response[$property]
            );
        }
    }

    public function testWillGetCities()
    {
        $this->createCities();
        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->getJson(
            '/api/v1/cities'
        );

        $this->assertSame(
            200,
            $response->status()
        );

        $decoded_response = $response->decodeResponseJson();

        $this->assertArrayHasKey('data', $decoded_response);
        $this->assertArrayHasKey('meta', $decoded_response);
        $this->assertSame(5, count($decoded_response['data']));
        $this->assertSame(5, $decoded_response['meta']['total']);
        $this->assertSame(5, count($decoded_response['data'][0]['comments']));
    }

    public function testWillGetCitiesLimitingComments()
    {
        $this->createCities();
        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->getJson(
                '/api/v1/cities?limit=1'
            );

        $this->assertSame(
            200,
            $response->status()
        );

        $decoded_response = $response->decodeResponseJson();

        $this->assertArrayHasKey('data', $decoded_response);
        $this->assertArrayHasKey('meta', $decoded_response);
        $this->assertSame(5, count($decoded_response['data']));
        $this->assertSame(5, $decoded_response['meta']['total']);
        $this->assertSame(1, count($decoded_response['data'][0]['comments']));
    }

    public function searchStringProvider()
    {
        return [
            [
                'Belgrade', 1
            ],
            [
                'Kikinda', 1
            ],
            [
                'London', 1
            ],
            [
                'l', 3
            ],
            [
                'ondo', 1
            ]
        ];
    }

    /**
     * @dataProvider searchStringProvider
     */
    public function testWillGetCityByName(
        string $search,
        int $find_count
    )
    {
        $this->createCities();
        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->getJson(
                "/api/v1/cities?name={$search}"
            );

        $this->assertSame(
            200,
            $response->status()
        );

        $decoded_response = $response->decodeResponseJson();

        $this->assertArrayHasKey('data', $decoded_response);
        $this->assertArrayHasKey('meta', $decoded_response);
        $this->assertSame($find_count, count($decoded_response['data']));
        $this->assertSame($find_count, $decoded_response['meta']['total']);
    }

    private function createCities()
    {
        City::insert([
            ['name' => 'Belgrade', 'country' => 'Serbia', 'description' => 'description'],
            ['name' => 'Kikinda', 'country' => 'Serbia', 'description' => 'description'],
            ['name' => 'Novi Sad', 'country' => 'Serbia', 'description' => 'description'],
            ['name' => 'London', 'country' => 'UK', 'description' => 'description'],
            ['name' => 'Barcelona', 'country' => 'Spain', 'description' => 'description'],
        ]);

        $city = City::all()->first();

        Comment::insert([
            [
                'city_id' => $city->id,
                'content' => 'TEST #1',
                'created_by_user_id' => $this->user->id,
            ],
            [
                'city_id' => $city->id,
                'content' => 'TEST #2',
                'created_by_user_id' => $this->user->id,
            ],
            [
                'city_id' => $city->id,
                'content' => 'TEST #3',
                'created_by_user_id' => $this->user->id,
            ],
            [
                'city_id' => $city->id,
                'content' => 'TEST #4',
                'created_by_user_id' => $this->user->id,
            ],
            [
                'city_id' => $city->id,
                'content' => 'TEST #5',
                'created_by_user_id' => $this->user->id,
            ],
        ]);
    }
}
