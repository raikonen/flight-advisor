<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticateTest extends ControllerTestCase
{
    use RefreshDatabase;

    public function testWillRegisterUser()
    {
        $user = User::all();
        $this->assertSame(
            0,
            count($user)
        );

        $response = $this->postJson('/api/register', [
            'username' => 'johndoe',
            'first_name' => 'John',
            'last_name' => 'Doe',
            'password' => 'easytoremmember',
        ]);

        $user = User::all()->first();

        $this->assertInstanceOf(
            User::class,
            $user
        );

        $this->assertSame(
            201,
            $response->status()
        );

        $this->assertResponse(
            $response,
            $user,
            ['id', 'username', 'first_name', 'last_name', 'api_token']
        );
    }

    public function testWillReturnBadRequestIfTryToRegisterUserWithExistingUsername()
    {
        $this->createUser();

        $response = $this->postJson('/api/register', [
            'username' => 'johndoe',
            'first_name' => 'John',
            'last_name' => 'Doe',
            'password' => 'easytoremmember',
        ]);

        $user = User::all()->first();

        $this->assertInstanceOf(
            User::class,
            $user
        );

        $this->assertSame(
            422,
            $response->status()
        );

        $this->assertErrorMessage(
            $response,
            ['username' => ["The username has already been taken."]]
        );
    }

    public function testWillLoginUser()
    {
        $this->createUser();

        $response = $this->postJson('/api/login', [
            'username' => 'johndoe',
            'password' => 'easytoremmember',
        ]);

        $user = User::all()->first();

        $this->assertSame(
            200,
            $response->status()
        );

        $this->assertInstanceOf(
            User::class,
            $user
        );

        $this->assertResponse(
            $response,
            $user,
            ['id', 'username', 'first_name', 'last_name', 'api_token']
        );
    }

    public function testWillLoginFailed()
    {
        $this->createUser();

        $response = $this->postJson('/api/login', [
            'username' => 'johndoe',
            'password' => 'easytoremmember2',
        ]);

        $user = User::all()->first();

        $this->assertInstanceOf(
            User::class,
            $user
        );

        $this->assertSame(
            422,
            $response->status()
        );

        $this->assertErrorMessage(
            $response,
            ['username' => ["These credentials do not match our records."]]
        );
    }

    public function testWillReturn401UnauthorisedAccess()
    {
        $response = $this->getJson('/api/v1/cities');

        $this->assertSame(
            401,
            $response->status()
        );
    }

    private function createUser()
    {
        User::create([
            'username' => 'johndoe',
            'first_name' => 'John',
            'last_name' => 'Doe',
            'password' => bcrypt('easytoremmember'),
        ]);

        $user = User::all();
        $this->assertSame(
            1,
            count($user)
        );

        return $user;
    }
}
