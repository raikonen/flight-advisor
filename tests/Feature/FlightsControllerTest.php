<?php

namespace Tests\Feature;

use App\City;
use App\Services\CheapestFlight\CheapestFlightFinderInterface;
use App\Services\ImportService\RoutesImporterInterface;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use LogicException;
use PHPUnit\Framework\MockObject\MockObject;

class FlightsControllerTest extends ControllerTestCase
{
    use RefreshDatabase;

    /**
     * @var RoutesImporterInterface|MockObject
     */
    private $flights_importer;

    /**
     * @var CheapestFlightFinderInterface|MockObject
     */
    private $chepest_flights;

    /**
     * @var string
     */
    private $api_token;
    private $user;
    private $city;

    protected function setUp(): void
    {
        parent::setUp();

        $this->api_token = Str::random(60);

        $this->user = User::create([
            'username' => 'johndoe',
            'first_name' => 'John',
            'last_name' => 'Doe',
            'password' => bcrypt('easytoremmember'),
        ]);

        $this->city = City::create([
            'name' => 'Belgrade',
            'country' => 'Serbia',
            'description' => 'description'
        ]);

        $this->city->wasRecentlyCreated = false;

        $this->user->api_token = $this->api_token;
        $this->user->role = User::ROLE_ADMIN;
        $this->user->save();

        $this->flights_importer = $this->createMock(RoutesImporterInterface::class);
        $this->chepest_flights = $this->createMock(CheapestFlightFinderInterface::class);

        $this->app->instance(
            RoutesImporterInterface::class,
            $this->flights_importer
        );

        $this->app->instance(
            CheapestFlightFinderInterface::class,
            $this->chepest_flights
        );
    }

    public function testWillReturnBadRequestIfFailedToFindCheapestFlight()
    {
        $this
            ->chepest_flights
            ->expects($this->once())
            ->method('find')
            ->with(
                $this->city,
                $this->city
            )->willThrowException(
                new LogicException('Source and destination cant be same.')
            );

        $response = $this
            ->withHeader('Authorization', "Bearer {$this->api_token}")
            ->postJson(
                "/api/v1/flights/cheapest",
                [
                    'source_id' => $this->city->id,
                    'destination_id' => $this->city->id,
                ]
            );

        $this->assertSame(
            400,
            $response->status()
        );

        $this->assertSame(
            'Source and destination cant be same.',
            $response->decodeResponseJson()
        );
    }
}
