<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFlights extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->id();
            $table->string('airline', 3)->nullable(false);
            $table->string('source_airport',4)->nullable(false);
            $table->bigInteger('source_airport_id')->nullable(false);
            $table->foreign('source_airport_id')->references('airport_id')->on('airports')->onDelete('cascade');
            $table->string('destination_airport',4)->nullable(false);
            $table->bigInteger('destination_airport_id')->nullable(false);
            $table->foreign('destination_airport_id')->references('airport_id')->on('airports')->onDelete('cascade');
            $table->char('codeshare')->nullable(true);
            $table->integer('stops')->default(0);
            $table->string('equipment', 3);
            $table->float('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_flights');
    }
}
