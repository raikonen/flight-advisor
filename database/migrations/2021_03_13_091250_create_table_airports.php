<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAirports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airports', function (Blueprint $table) {
            $table->bigInteger('airport_id')->primary();
            $table->string('name');
            $table->bigInteger('city_id');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->string('country');
            $table->string('iata', 3)->nullable(true)->default(null);
            $table->string('icao', 4)->nullable(true)->default(null);
            $table->double('latitude');
            $table->double('longitude');
            $table->float('altitude');
            $table->integer('timezone');
            $table->string('dts');
            $table->string('tz');
            $table->string('type');
            $table->string('source');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_airports');
    }
}
