<?php

use App\User;
use Illuminate\Database\Seeder;

class SeedAdmin extends Seeder
{
    public function run()
    {
        $user = User::create([
            'username' => 'admin',
            'first_name' => 'Admin',
            'last_name' => 'Adminovic',
            'password' => bcrypt('admin1234567'),
        ]);

        $user->role = User::ROLE_ADMIN;
        $user->save();
    }
}
