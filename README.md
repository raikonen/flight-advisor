## SETUP PROJECT
Run composer:

```
composer install
```

Create `database.sqlite` file in `/database` directory if  not exists.

Copy `.env.example`file rename it to `.env` and edit as following: 

```
DB_CONNECTION=sqlite
#DB_HOST=127.0.0.1
#DB_PORT=3306
#DB_DATABASE=laravel
#DB_USERNAME=root
#DB_PASSWORD=
```

Run migrations:
```
php artisan migrate
```

Run seed to create admin user (username `admin`, password `admin1234567`):
```
php artisan db:seed
```

Run server
```
php artisan serve
```
Application is running.. 

## Routes

Application use header `Autorisation` `Bearer token` for authentication.

<table>
    <thead>
        <th>route</th>
        <th>method</th>
        <th>attributes</th>
        <th>description</th>
    </thead>
    <tbody>
        <tr>
            <td>/api/login</td>
            <td>POST</td>
            <td>username, password</td>
            <td> Return user if exist and <strong>token</strong></td>
        </tr>      
         <tr>
            <td>/api/logout</td>
            <td>POST</td>
            <td>/</td>
            <td> Unset user api token</td>
        </tr>
        <tr>
            <td>/api/register</td>
            <td>POST</td>
            <td>username, first_name, last_name, password</td>
            <td>Create user </td>
        </tr>     
        <tr>
            <td>/api/v1/airports/import</td>
            <td>POST</td>
            <td>import - file.txt</td>
            <td>Imports airports data</td>
        </tr>    
        <tr>
            <td>/api/v1/cities/{id}/airports</td>
            <td>POST</td>
            <td> airport_id, name, city_id, country,
                iata, icao, latitude, longitude,
                altitude, timezone, dts, tz, type,
                source
            </td>
            <td>Create airport in city</td>
        </tr>    
        <tr>
            <td>/api/v1/flights/import</td>
            <td>POST</td>
            <td>import - file.txt</td>
            <td>Imports routes data</td>
        </tr>         
         <tr>
            <td>/api/v1/flights</td>
            <td>POST</td>
            <td> airport_id, name, city_id, 
                 country, iata, icao, latitude, 
                 longitude, altitude, timezone, 
                 dts, tz, type, source,
            </td>
            <td>Create flight</td>
        </tr>       
        <tr>
            <td>/api/v1/flights/cheapest</td>
            <td>POST</td>
            <td>source_id, destination_id (city id)</td>
            <td>Return cheapest rute from source city to destination city</td>
        </tr>
        <tr>
            <td>/api/v1/cities</td>
            <td>POST</td>
            <td>name, country, description</td>
            <td>Create city</td>
        </tr>
        <tr>
            <td>/api/v1/cities</td>
            <td>GET</td>
            <td></td>
            <td>Return cities with all comments (paginated 10). if <strong>name</strong> query param is provided return cities that contains name. If <strong>limit</strong> query param is provided returns cities with latest limit comments</td>
        </tr>
        <tr>
            <td>api/v1/cities/{id}/comments</td>
            <td>POST</td>
            <td>content</td>
            <td>Create comments on city</td>
        </tr>
        <tr>
            <td>api/v1/comments/{id}</td>
            <td>PUT</td>
            <td>content</td>
            <td>Edit comment</td>
        </tr>
        <tr>
            <td>api/v1/comments/{id}</td>
            <td>DELETE</td>
            <td></td>
            <td>Delete comment</td>
        </tr>
    </tbody>
</table>
