<?php

namespace App\Http\Controllers;

use App\City;
use App\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store(int $id, Request $request)
    {
        $city = City::findOrFail($id);

        $request->validate([
            'content' => 'required',
        ]);

        $comment = Comment::create([
            'city_id' => $city->id,
            'content' => $request->input('content'),
            'created_by_user_id' => $request->user()->id,
        ]);

        return response()
            ->json(
                $comment,
            201
            );
    }

    public function update(int $id, Request $request)
    {
        $comment = Comment::findOrFail($id);

        if ($request->user()->id !== (int) $comment->created_by_user_id) {
            return response()
                ->json(
                    'Only creator can modify comment',
                    403
                );
        }

        $comment->update($request->all());

        return response()
            ->json(
                $comment,
                200
            );
    }

    public function destroy(int $id, Request $request)
    {
        $comment = Comment::findOrFail($id);

        if ((int) $request->user()->id !== (int) $comment->created_by_user_id) {
            return response()
                ->json(
                    'Only creator can delete comment',
                    403
                );
        }

        $comment->delete();

        return response()
            ->json(
                null,
                204
            );
    }
}
