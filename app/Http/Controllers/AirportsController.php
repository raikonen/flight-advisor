<?php

namespace App\Http\Controllers;

use App\Airport;
use App\City;
use App\Services\ImportService\AirportsImporterInterface;
use Illuminate\Http\Request;

class AirportsController extends Controller
{
    private $importer;

    public function __construct(
        AirportsImporterInterface $importer
    ) {
        $this->importer = $importer;
    }

    public function import(Request $request)
    {
        $request->validate([
            'import' => 'required|file|max:2000|mimes:txt' //upload_max_filesize
        ]);

        $file = $request->file('import');

        if (!$file->isValid()) {
            return response('Not valid file.', 400);
        }

        $this
            ->importer
            ->import(
                $file->openFile('r')
            );

        return response()
            ->json(
                null,
                200
            );
    }

    public function store(int $id, Request $request)
    {
        $request->validate([
            'airport_id'=> 'required|unique:airports',
            'name' => 'required',
            'country' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'altitude' => 'required',
            'timezone' => 'required',
            'dts' => 'required',
            'tz' => 'required',
            'type' => 'required',
            'source' => 'required',
        ]);

        $city = City::findOrFail($id);

        $data = $request->all();
        $data['city_id'] = $city->id;

        $airport = Airport::create($data);

        return response()
            ->json(
                $airport,
                201
            );
    }
}
