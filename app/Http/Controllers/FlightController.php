<?php

namespace App\Http\Controllers;

use App\Airport;
use App\City;
use App\Flight;
use App\Services\CheapestFlight\CheapestFlightFinderInterface;
use App\Services\ImportService\RoutesImporterInterface;
use Exception;
use Illuminate\Http\Request;

class FlightController extends Controller
{
    private $importer;
    private $cheapest_flight;

    public function __construct(
        RoutesImporterInterface $importer,
        CheapestFlightFinderInterface $cheapest_flight
    ) {
        $this->importer = $importer;
        $this->cheapest_flight = $cheapest_flight;
    }

    public function import(Request $request)
    {
        $request->validate([
            'import' => 'required|file|max:2000|mimes:txt' //upload_max_filesize
        ]);

        $file = $request->file('import');

        if (!$file->isValid()) {
            return response('Not valid file.', 400);
        }

        $this
            ->importer
            ->import(
                $file->openFile('r')
            );

        return response()
            ->json(
                null,
                200
            );
    }

    public function cheapest(
        Request $request
    ) {
        $request->validate([
            'source_id' => 'required',
            'destination_id' => 'required',
        ]);

        try {
            $source = City::findOrFail((float)$request->input('source_id'));
            $destination = City::findOrFail((float)$request->input('destination_id'));

            return $this
                ->cheapest_flight
                ->find(
                    $source,
                    $destination
                );
        } catch (Exception $exception) {
            return response()
                ->json(
                    $exception->getMessage(),
                    400
                );
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'airline' => 'required',
            'source_airport' => 'required',
            'source_airport_id' => 'required',
            'destination_airport' => 'required',
            'destination_airport_id' => 'required',
            'codeshare' => 'required',
            'stops' => 'required',
            'equipment' => 'required',
            'price' => 'required',
        ]);

        $source = Airport::findOrFail((float) $request->input('source_airport_id'));
        $destination = Airport::findOrFail((float) $request->input('destination_airport_id'));

        $data = $request->all();
        $data['source_airport_id'] = $source->airport_id;
        $data['destination_airport_id'] = $destination->airport_id;

        $flight = Flight::create($data);

        return response()
            ->json(
                $flight,
                201
            );
    }
}
