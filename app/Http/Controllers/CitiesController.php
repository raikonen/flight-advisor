<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Resources\CityCollection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;

class CitiesController extends Controller
{
    public function index(Request $request)
    {
        $limit = null;
        if ($request->has('limit')) {
            $limit = (int) $request->get('limit');
        }

        $name = null;
        if ($request->has('name')) {
            $name = $request->input('name');
        }

        $cities = City::with($this->getCommentsQuery($limit));

        if ($name) {
            $cities = $cities
                ->where(
                    'name',
                    'like',
                    "%{$request->input('name')}%"
                );
        }

        return new CityCollection($cities->paginate(10));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:cities',
            'country' => 'required',
            'description' => 'required',
        ]);

        $city = City::create($request->all());

        return response()
            ->json(
                $city,
                201
            );
    }

    private function getCommentsQuery(?int $limit)
    {
        return !empty($limit) ? ['comments' => function(HasMany $query) use ($limit) {
            return $query->latest()->limit($limit);
        }] : 'comments';
    }
}
