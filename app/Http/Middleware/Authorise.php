<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class Authorise
{
    public function handle($request, Closure $next)
    {
        if ($request->user()->role !== User::ROLE_ADMIN) {
            abort(403, 'Unauthenticated');
        }

        return $next($request);
    }
}
