<?php

namespace App;

use App\Utils\DistanceCalculator;
use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $table = 'flights';
    protected $primaryKey = 'id';
    public $incrementing = true;

    protected $fillable = [
        'airline',
        'source_airport',
        'source_airport_id',
        'destination_airport',
        'destination_airport_id',
        'codeshare',
        'stops',
        'equipment',
        'price',
    ];

    public function destinationAirport()
    {
        return $this->hasOne(Airport::class, 'airport_id', 'destination_airport_id');
    }

    public function sourceAirport()
    {
        return $this->hasOne(Airport::class, 'airport_id', 'source_airport_id');
    }

    public function getDistanceAttribute()
    {
        return (new DistanceCalculator())->getDistance($this->destinationAirport, $this->sourceAirport);
    }

    public function jsonSerialize()
    {
        return [
            'source' => $this->sourceAirport->city->name,
            'destination' => $this->destinationAirport->city->name,
            'price' => $this->price,
        ];
    }
}
