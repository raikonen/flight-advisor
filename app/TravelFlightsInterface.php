<?php

namespace App;

interface TravelFlightsInterface
{
    public function getFlights(): array;
    public function addFlight(Flight $flight): void;
    public function getPrice();
    public function getDistance();
}
