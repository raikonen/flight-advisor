<?php

namespace App;

interface CoordinateInterface
{
    public function latitude();

    public function longitude();
}
