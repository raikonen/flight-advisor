<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model implements CoordinateInterface
{
    protected $table = 'airports';
    protected $primaryKey = 'airport_id';
    public $incrementing = true;

    protected $fillable = [
        'airport_id',
        'name',
        'city_id',
        'country',
        'iata',
        'icao',
        'latitude',
        'longitude',
        'altitude',
        'timezone',
        'dts',
        'tz',
        'type',
        'source',
    ];

    public function latitude()
    {
        return $this->latitude;
    }

    public function longitude()
    {
        return $this->longitude;
    }

    public function departingFlights()
    {
        return $this->hasMany(Flight::class, 'source_airport_id');
    }

    public function arrivingFlights()
    {
        return $this->hasMany(Flight::class, 'destination_airport_id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }
}
