<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use phpDocumentor\Reflection\Types\Self_;

class User extends Authenticatable
{
    use Notifiable;

    const ROLE_REGULAR = 'regular';
    const ROLE_ADMIN = 'admin';
    const ROLES = [
        self::ROLE_REGULAR,
        self::ROLE_ADMIN,
    ];

    protected $fillable = [
        'username',
        'first_name',
        'last_name',
        'password',
    ];

    protected $hidden = [
        'password',
        'salt',
    ];
}
