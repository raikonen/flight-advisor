<?php

namespace App\Providers;

use App\Airport;
use App\City;
use App\Flight;
use App\Services\CheapestFlight\CheapestFlightFinder;
use App\Services\CheapestFlight\CheapestFlightFinderInterface;
use App\Services\ImportService\AirportsImporter;
use App\Services\ImportService\AirportsImporterInterface;
use App\Services\ImportService\RoutesImporter;
use App\Services\ImportService\RoutesImporterInterface;
use App\Utils\DistanceCalculator;
use App\Utils\DistanceCalculatorInterface;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(DistanceCalculatorInterface::class, function () {
            return new DistanceCalculator();
        });

        $this->app->bind(CheapestFlightFinderInterface::class, function ($app) {
            return new CheapestFlightFinder(
                $app->make(DistanceCalculatorInterface::class)
            );
        });

        $this->app->bind(AirportsImporterInterface::class, function () {
            return new AirportsImporter(
                function (array $airports) {
                    Airport::insertOrIgnore($airports);
                },
                function () {
                    return City::all()->keyBy('name')->toArray();
                }
            );
        });

        $this->app->bind(RoutesImporterInterface::class, function () {
            return new RoutesImporter(
                function () {
                    return Airport::all()->keyBy('airport_id')->toArray();
                },
                function (array $flights) {
                    Flight::insertOrIgnore($flights);
                }
            );
        });
    }

    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
