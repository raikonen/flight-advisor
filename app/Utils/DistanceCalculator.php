<?php


namespace App\Utils;


use App\CoordinateInterface;

class DistanceCalculator implements DistanceCalculatorInterface
{
    public function getDistance(CoordinateInterface $destination, CoordinateInterface $source): float
    {
        if (($destination->latitude() == $source->latitude())
            && ($destination->longitude() == $source->longitude())
        ) {
            return 0.0;
        }

        $theta = $destination->longitude() - $source->longitude();
        $dist = sin(deg2rad($destination->latitude())) * sin(deg2rad($source->latitude()))
            + cos(deg2rad($destination->latitude())) * cos(deg2rad($source->latitude())) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);

        return round($dist * 60 * 1.1515 * 1.609344, 2);
    }
}
