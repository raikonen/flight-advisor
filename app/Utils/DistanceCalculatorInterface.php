<?php

namespace App\Utils;

use App\CoordinateInterface;

interface DistanceCalculatorInterface
{
    public function getDistance(
        CoordinateInterface $destination,
        CoordinateInterface $source
    ): float;
}
