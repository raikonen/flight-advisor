<?php


namespace App\Services\ImportService;


use SplFileObject;

class RoutesImporter implements RoutesImporterInterface
{
    private $airports_finder;
    private $flights_repository;
    private $airports;

    public function __construct(
        callable $airports_finder,
        callable $flights_repository
    ) {
        $this->airports_finder = $airports_finder;
        $this->flights_repository = $flights_repository;
    }

    public function import(SplFileObject $file)
    {
        $this->airports = call_user_func($this->airports_finder);

        $routes = [];
        while ($data = $file->fgetcsv()) {
            if ($this->isSetAirportsData($data) && isset($this->airports[$data[3]]) && isset($this->airports[$data[5]])) {
                $routes[] = [
                    'airline' => $data[0],
                    'source_airport' => $data[2],
                    'source_airport_id' => $this->airports[$data[3]]['airport_id'],
                    'destination_airport' => $data[4],
                    'destination_airport_id' => $this->airports[$data[5]]['airport_id'],
                    'codeshare' => $data[6],
                    'stops' => $data[7],
                    'equipment' => $data[8],
                    'price' => $data[9],
                ];
            }
        }

        if (!empty($routes)) {
            foreach (array_chunk($routes, 100) as $chunk) {
                call_user_func(
                    $this->flights_repository,
                    $chunk
                );
            }
        }
    }

    private function isSetAirportsData(array $data): bool
    {
        return isset($data[3]) && isset($data[5]);
    }
}
