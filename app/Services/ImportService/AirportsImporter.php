<?php

namespace App\Services\ImportService;

use SplFileObject;

class AirportsImporter implements AirportsImporterInterface
{
    private $airport_repository;
    private $cities;
    private $cities_finder;

    public function __construct(
        callable $airport_repository,
        callable $cities_finder
    ) {
        $this->airport_repository = $airport_repository;
        $this->cities_finder = $cities_finder;
    }

    public function import(
        SplFileObject $file
    ) {
        $this->cities = call_user_func($this->cities_finder);

        $airports = [];
        while ($data = $file->fgetcsv()) {
            if ($this->isCityAndCountry($data) && isset($this->cities[$data[2]])) {
                $airports[] = [
                    'airport_id' => $data[0],
                    'name' => $data[1],
                    'city_id' => $this->cities[$data[2]]['id'],
                    'country' => $data[3],
                    'iata' => $data[4],
                    'icao' => $data[5],
                    'latitude' => (double) $data[6],
                    'longitude' => (double) $data[7],
                    'altitude' => (float) $data[8],
                    'timezone' => $data[9],
                    'dts' => $data[10],
                    'tz' => $data[11],
                    'type' => $data[12],
                    'source' => $data[13],
                ];
            }
        }

        if (!empty($airports)) {
            foreach (array_chunk($airports, 50) as $chunk) {
                call_user_func(
                    $this->airport_repository,
                    $chunk
                );
            }
        }
    }

    private function isCityAndCountry(array $data): bool
    {
        return !empty($data[2]) && !empty($data[3]);
    }
}
