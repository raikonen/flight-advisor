<?php

namespace App\Services\ImportService;

use SplFileObject;

interface AirportsImporterInterface
{
    public function import(
        SplFileObject $file
    );
}
