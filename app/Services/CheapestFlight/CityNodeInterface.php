<?php

namespace App\Services\CheapestFlight;

use App\City;
use App\Flight;

interface CityNodeInterface
{
    public function getID();

    public function getCity(): City;

    public function getFlight(): ?Flight;
}
