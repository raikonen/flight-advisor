<?php

namespace App\Services\CheapestFlight;

use App\City;
use App\Flight;
use JMGQ\AStar\AbstractNode;

class CityNode extends AbstractNode implements CityNodeInterface
{
    private $city;
    private $flight;

    public function __construct(
        City $city,
        ?Flight $flight = null
    ) {
        $this->city = $city;
        $this->flight = $flight;
    }

    public function getID()
    {
        return $this->city->id;
    }

    public function getCity(): City
    {
        return $this->city;
    }

    public function getFlight(): ?Flight
    {
        return $this->flight;
    }
}
