<?php

namespace App\Services\CheapestFlight;

use App\City;
use App\TravelFlightsInterface;

interface CheapestFlightFinderInterface
{
    public function find(City $source, City $destination): TravelFlightsInterface;
}
