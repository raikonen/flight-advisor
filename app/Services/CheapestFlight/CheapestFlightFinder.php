<?php

namespace App\Services\CheapestFlight;

use App\Airport;
use App\City;
use App\Flight;
use App\TravelFlights;
use App\Utils\DistanceCalculator;
use App\Utils\DistanceCalculatorInterface;
use JMGQ\AStar\AStar;
use JMGQ\AStar\Node;
use LogicException;
use \App\TravelFlightsInterface;

class CheapestFlightFinder extends AStar implements CheapestFlightFinderInterface
{
    private $calculator;

    public function __construct(DistanceCalculatorInterface $calculator)
    {
        $this->calculator = $calculator;
    }

    public function generateAdjacentNodes(Node $node)
    {
        //@ToDo refactor
        $nodes = [];
        $airports = $node->getCity()->airports;

        if (empty($airports)) {
            return  [];
        }

        foreach ($airports as $airport) {
            foreach ($airport->departingFlights as $flight) {
                $nodes[] = new CityNode(
                    $flight->destinationAirport->city,
                    $flight
                );
            }
        }

        return $nodes;
    }

    public function calculateRealCost(Node $node, Node $adjacent)
    {
        return $adjacent->getFlight()->distance;
    }

    public function calculateEstimatedCost(Node $start, Node $end)
    {
        return $this->calculator->getDistance(
            $end->getCity()->airports->first(),
            $start->getCity()->airports->first()
        );
    }

    public function find(
        City $source,
        City $destination
    ): TravelFlightsInterface
    {
        if ($source->id === $destination->id) {
            throw new LogicException('Source and destination cant be same.');
        }

        if (empty($source->airports)) {
            throw new LogicException('There is to airport in source city.');
        }

        if (empty($destination->airports)) {
            throw new LogicException('There is to airport in destination city.');
        }

        $nodes = $this->run(
            new CityNode($source),
            new CityNode($destination)
        );

        $patch = new TravelFlights();

        /** @var CityNode $node */
        foreach ($nodes as $node) {
            if ($node->getFlight()) {
                $patch->addFlight($node->getFlight());
            }
        }

        return $patch;
    }
}
