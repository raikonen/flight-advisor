<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    protected $primaryKey = 'id';
    public $incrementing = true;

    protected $fillable = [
        'name',
        'country',
        'description',
    ];

    public function airports()
    {
        return $this->hasMany(Airport::class, 'city_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'city_id');
    }
}
