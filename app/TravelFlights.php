<?php

namespace App;

use JsonSerializable;

class TravelFlights implements TravelFlightsInterface, JsonSerializable
{
    private $flights = [];
    private $price = null;
    private $distance = null;

    public function getFlights(): array
    {
        return $this->flights;
    }

    public function addFlight(Flight $flight): void
    {
        $this->flights[] = $flight;
        $this->price += $flight->price;
        $this->distance += $flight->distance;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getDistance()
    {
        return $this->distance;
    }

    public function jsonSerialize()
    {
        return [
            'route' => $this->flights,
            'total' => round($this->price, 2),
            'distance' => round($this->distance, 2),
        ];
    }
}
